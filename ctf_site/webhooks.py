import json

import requests

from flask import current_app as app


def send_webhook(status, title, content="", colour=0xFF5500, fields=None):
    pass
    avatar = "https://cdn.discordapp.com/attachments/290757101914030080/576322387902070784/logo.png"
    if fields is None:
        fields = []

    data = dict(
        username="CDCTF - " + status,
        avatar_url=avatar,
        embeds=[dict(title=title, description=content, color=colour, fields=fields)],
    )

    result = requests.post(
        app.config["webhook_url"],
        data=json.dumps(data),
        headers={"Content-Type": "application/json"},
    )

    try:
        result.raise_for_status()
    except requests.exceptions.HTTPError as err:
        print("Error sending embed - " + str(err))
        print(result.content)
