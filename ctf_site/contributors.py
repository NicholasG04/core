contributors_dict = {
    "art": [
        "BenTechy66",
        "Kernel Panic"
    ],
    "audio": [
        "BenTechy66",
        "lightspeedana"
    ],
    "challenges": {
        "Bottersnike": [
            "Peculiar Post-it",
            "In plain sight",
            "Trippy Packets",
            "Text trap",
            "A Crafty Socket",
            "Fricking weebs",
            "The Flag, Luke",
            "Lots of Super Beats",
            "HackerChat",
            "Crawling w/ Excitement",
            "BTCap",
            "Python Game"
        ],
        "NateKomodo": [
            "Brute force",
            "Wrapped in an enigma",
            "Weakest link",
            "IL Part 1",
            "IL Part 2",
            "Under you",
            "Transmission Received",
            "Spooky Mixtapes",
            "Obscurity"
        ],
        "BenTechy66": [
            "I am a robot",
            "HackerPlayer",
            "HashCracked4U",
            "Ternary",
            "EcoSh",
            "Scrabble Scramble",
            "WeTube",
            "Madhouse"
        ],
        "Dave": [
            "Secured",
            "Troublesome",
            "Missing Digits",
            "Vulnerable",
            "Decaf",
            "lesson4chiv"
        ],
        "Linus": [
            "Intercepted Email",
            "Wireless Infidelity",
            "Take Me Home"
        ],
        "ubq323": [
            "nfalqxis",
            "emojasm"
        ],
        "Alphex": [
            "Raw data"
        ],
        "Rowan": [
            "LyneSteg"
        ],
        "Swim": [
            "Queen.zip"
        ],
        "HurricanKai": [
            "Introduction to DN"
        ],
        "SSAFuze": [
            "Genesis"
        ]
    },
    "programming": [
        "BenTechy66",
        "NateKomodo",
        "ZomBMage",
        "lightspeedana",
        "Nick",
        "KernelPanic",
        "Bottersnike"
    ]
}
