from flask import Blueprint, render_template, request, jsonify

from .util.decorators import admin_only, login_required
from .webhooks import send_webhook
from .models import Ticket, User, db, TicketMessage
from .config import admins
from .util.limiter import limiter

support = Blueprint("support", __name__, template_folder="templates", url_prefix="/support")

@support.route("/")
@login_required
def support_index(user):
    return render_template("support/client_index.html", user=user)


@support.route("/admin/")
@admin_only
def admin_index():
    return render_template("support/admin_index.html")


@support.route("/admin/open/")
@admin_only
def admin_open_tickets():
    return render_template("support/admin_tickets.html", tickets=Ticket.query.filter_by(ticket_status="Open"))


@support.route("/admin/all/")
@admin_only
def admin_all_tickets():
    return render_template("support/admin_tickets.html", tickets=Ticket.query.all())


@support.route("/tickets/")
@login_required
def user_tickets(user):
    return render_template("support/client_tickets.html", user=user, tickets=Ticket.query.filter_by(
        user_id=user.discord_user_id
        ).all()
    )


@support.route("/tickets/<id>", methods=["GET"])
@login_required
def view_ticket(user, id):
    admin = False
    if request.args.get('admin') is "1":
        admin = True
    if id == "new":
        return render_template("support/new_ticket.html", user=user)
    else:
        ticket = Ticket.query.filter_by(ticket_id=id).first()
        if not ticket:
            return render_template("pages/error.html",
                                   msg="That support ticket does not exist.")
        if not ticket.user_id == user.discord_user_id and user.discord_user_id not in admins:
            return render_template("pages/error.html",
                                   msg="You do not have permission to view this support ticket.")
        ticket.messages = TicketMessage.query.filter_by(ticket_id=id).order_by(TicketMessage.message_num.asc())
        return render_template("support/ticket.html", user=user, ticket=ticket, admin=admin)

@support.route("/tickets/new", methods=["POST"])
@login_required
@limiter.limit("1/minute")
def new_ticket(user):
    data = request.form
    num_tickets = len(Ticket.query.all())
    if data["title"] is "" or data["title"] is None or data["body"] is "" or data["body"] is None:
        return jsonify({"s": False, "m": "Cannot create a ticket with blank fields."})
    
    db.session.add(Ticket(user_id=user.discord_user_id,
                          ticket_status="Open",
                          ticket_id=num_tickets + 1,
                          ticket_title=data["title"]))
    db.session.add(TicketMessage(author_id=user.discord_user_id,
                                 author_name=user.display_name,
                                 message_content=data["body"],
                                 ticket_id=num_tickets + 1,
                                 message_num=1))
    db.session.commit()
    send_webhook("Support", "New Support Ticket", f"User {user.display_name} created support ticket #{num_tickets + 1}")
    return jsonify({"s": True, "m": "Ticket created.", "r": f"./{num_tickets + 1}"})
        

@support.route("/tickets/<id>", methods=["POST"])
@login_required
@limiter.limit("4/minute")
def update_ticket(user, id):
    data = request.form
    ticket = Ticket.query.filter_by(ticket_id=id).first()
    if data["body"] is "" or data["body"] is None:
        return jsonify({"s": False, "m": "Cannot create a ticket with blank fields."})
    
    if not ticket:
        return jsonify({"s": False, "m": "That ticket does not exist."})

    if not ticket.user_id == user.discord_user_id and user.discord_user_id not in admins:
        return jsonify({"s": False, "m": "You do not have permissions to update that ticket."})

    if not ticket.ticket_status == "Open" and user.discord_user_id not in admins:
        return jsonify({"s": False, "m": "Cannot update a non-open ticket."})

    num_messages = len(TicketMessage.query.filter_by(ticket_id=id).all())

    db.session.add(TicketMessage(author_id=user.discord_user_id,
                                    author_name=user.display_name,
                                    message_content=data["body"],
                                    ticket_id=id,
                                    message_num=num_messages + 1))
    db.session.commit()

    send_webhook("Support", "Ticket Updated", f"User {user.display_name} updated support ticket #{id}")
    return jsonify({"s": True, "m": "Ticket updated."})

@support.route("/tickets/<id>/close", methods=["POST"])
@login_required
def close_ticket(user, id):
    data = request.form
    user = User.query.filter_by(auth_token=data["auth"]).first()
    if not user:
        return jsonify({"s": False, "m": "Invalid authentication"})
    ticket = Ticket.query.filter_by(ticket_id=id).first()
    if not ticket:
        return jsonify({"s": False, "m": "That ticket does not exist."})

    if not ticket.user_id == user.discord_user_id and user.discord_user_id not in admins:
        return jsonify({"s": False, "m": "You do not have permissions to update that ticket."})

    ticket.ticket_status = "Closed"
    db.session.commit()
    send_webhook("Support", "Ticket Closed", f"User {user.display_name} closed support ticket #{id}")
    return jsonify({"s": True, "m": "Ticket closed."})



