import configparser
import json
import os

# This file performs dynamic config loading logic.
#
# Because of how python caches imports, we don't need to worry about
# this file being imported multiple times.

# Check if we actually have a config to load
if not os.path.exists("config.ini"):
    print()
    print("-> No configuration file exists!")
    # If we have a default config available...
    if os.path.exists("config.default.ini"):
        # ... clone the default config...
        with open("config.default.ini") as default:
            with open("config.ini", "w") as new:
                new.write(default.read())
        # ... and tell the user!
        print("-> A new config has been created from config.default.ini")

    # Even if we reloaded the default, it's going to need
    # tweeking
    print("-> You should configure the server before attempting to start it")
    quit()

# Load from config.ini
config = configparser.ConfigParser()
config.read("config.ini")

# Check if we're actually loading a default config
try:
    config.get("Config", "is_default")
except configparser.NoOptionError:
    pass
except configparser.NoSectionError:
    pass
else:
    print()
    print("-> The loaded config appears to be a default config!")
    print("-> Did you remember to remove is_default?")
    quit()

# [Server]
port = config.getint("Server", "port")
bind_ip = config.get("Server", "bind_ip")
public_address = config.get("Server", "public_address")

countdown = config.getint("Server", "countdown")

# [Admin]
admins = json.loads(config.get("Admin", "admins"))
beta_testers = json.loads(config.get("Admin", "beta_testers"))

# [Discord]
webhook_url = config.get("Discord", "webhook_url")
client_id = config.get("Discord", "client_id")
client_secret = config.get("Discord", "client_secret")

# [Formatting]
formatting = dict(config["Formatting"])


# Flask config object
class Config:
    port = port
    ip = bind_ip
    SQLALCHEMY_DATABASE_URI = "sqlite:///main.db"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = os.urandom(32)
    webhook_url = webhook_url


# Clean up for when import *'ing
if __name__ != "__main__":
    del os, json, configparser, config
