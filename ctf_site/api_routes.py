import time
import uuid
import os
from datetime import datetime
from secrets import token_hex
from string import printable

import requests

from flask import Blueprint, abort, jsonify, make_response, redirect, request, url_for, send_from_directory
from flask_socketio import emit

from .util.decorators import admin_only, login_required
from .util.hashing import sha256_digest
from .util.limiter import limiter
from .util.oauth import OAuth
from .util.paths import resolve_path
from .webhooks import send_webhook
from .models import Challenge, User, SolveTimeModel, db
from .config import admins, formatting, beta_testers
from .csrf import csrf

from .leaderboard import leaderboard

api = Blueprint("api", __name__, template_folder="templates", url_prefix="/api")
ipn = Blueprint("ipn", __name__, template_folder="templates", url_prefix="/api")
limiter.limit("1/second")(api)

alerts = []
news = [] #So if we remove alert it doesnt vanish from news

AVATARS = resolve_path("cache")
os.makedirs(AVATARS, exist_ok=True)

# 10 Days
NEW_THRESHOLD = 10 * 24 * 24 * 60 * 1000


def update_leaderboard():
    emit("leaderboard", leaderboard.get_lbdata(), namespace="/", broadcast=True)


def remove_old_avatar(avatar):
    if avatar + ".png" in os.listdir(AVATARS):
        os.remove(os.path.join(AVATARS, avatar + ".png"))


def get_new_avatar(uid, avatar):
    if avatar is None or avatar is "default":
        return
    url = f"https://cdn.discordapp.com/avatars/{uid}/{avatar}.png?size=256"
    r = requests.get(url)
    with open(os.path.join(AVATARS, avatar + ".png"), "wb") as f:
        f.write(r.content)


@api.route("/pointsGraph")
@csrf.exempt
def points_graph():
    return jsonify(leaderboard.points_graph)

    data = []

    for n, user in enumerate(User.query.all()):
        plot = {'x': [], 'y': [], 'name': str(user.display_name), 'mode': 'lines+markers'}
        points = 0
        mtime = 0
        for solve in SolveTimeModel.query.filter_by(user_id=user.discord_user_id).order_by(SolveTimeModel.solve_time.asc()).all():
            points += Challenge.query.filter_by(challenge=solve.challenge, category=solve.category).first().points
            plot['x'].append(solve.solve_time.isoformat())
            plot['y'].append(points)

            mtime = max(mtime, (1<<32) - datetime.timestamp(solve.solve_time))
        # Sort by points, then inverse time, then a backup int to avoid sorting by a dict
        data.append((points, mtime, n, plot))
    data.sort(reverse=True)

    # Top 15 players
    return jsonify([i[-1] for i in data[:15]])

@api.route("/avatar/<avatar>")
def get_avatar(avatar):
    if avatar is None or avatar is "default":
        return None #TODO return a default or smthn
    return send_from_directory(AVATARS, avatar + ".png")

@ipn.route("/ipn", methods=["GET","POST"])
@csrf.exempt
def ipn_handler():
    arg = ''
    values = request.form
    for x,y in values.items():
        arg += f"&{x}={y}"

    r = requests.get(f"https://ipnpb.paypal.com/cgi-bin/webscr?cmd=_notify-validate{arg}")
    if r.text == "VERIFIED":
        data = {}

        data["donation_amount"] = values.get("mc_gross")
        data["donation_date"] = values.get("payment_date")
        data["first_name"] = values.get("first_name")
        data["last_name"] = values.get("last_name")
        data["email"] = values.get("payer_email")
        data["transaction_id"] = values.get("txn_id")
        data["receipt_id"] = values.get("receipt_id")
        discord_id = values.get("custom")

        fields_for_webhook = []

        donated_event = {}
        donated_event["donation_amount"] = "£" + data["donation_amount"]

        if discord_id is not None:
            user = User.query.filter_by(discord_user_id=discord_id).first().display_name
            amount = data["donation_amount"]
            transaction = data["transaction_id"]
            receipt = data["receipt_id"]
            fields_for_webhook.append({"inline": True, "name": "Name", "value": user})
            fields_for_webhook.append({"inline": True, "name": "Amount", "value": f"£{amount}"})
            fields_for_webhook.append({"inline": True, "name": "Transaction ID", "value": f"{transaction}"})
            fields_for_webhook.append({"inline": True, "name": "Receipt ID", "value": f"{transaction}"})
            send_webhook(
                "Donation",
                f"{user}, <@{discord_id}> has donated £{data['donation_amount']}!",
                fields=fields_for_webhook)
            donated_event["anonymous"] = False
            donated_event["nickname"] = User.query.filter_by(discord_user_id=discord_id).first().display_name
            if float(data["donation_amount"]) >= 1.00:
                emit("donate", donated_event, namespace="/", broadcast=True)
                User.query.filter_by(discord_user_id=discord_id).first().isdonor = True
                db.session.commit()
        else:
            fields_for_webhook.append({"inline": True, "name": "Name", "value": "Anonymous"})
            amount = data["donation_amount"]
            transaction = data["transaction_id"]
            receipt = data["receipt_id"]
            fields_for_webhook.append({"inline": True, "name": "Amount", "value": f"£{amount}"})
            fields_for_webhook.append({"inline": True, "name": "Transaction ID", "value": f"{transaction}"})
            fields_for_webhook.append({"inline": True, "name": "Receipt ID", "value": f"{transaction}"})
            send_webhook(
                "Donation",
                f"Anonymous donation of £{data['donation_amount']}!",
                fields=fields_for_webhook)
            donated_event["anonymous"] = True
            if float(data["donation_amount"]) >= 1.00:
                emit("donate", donated_event, namespace="/", broadcast=True)
        return "", 200
    else:
        return redirect("/home")

@api.route("/login", methods=["GET"])
def discordlogin():
    """Return URL from discord OAUTH"""
    code = request.args.get("code")
    token = OAuth.get_access_token(code)
    user_json = OAuth.get_user_json(token)
    userid = user_json.get("id")
    username = user_json.get("username")
    discrim = user_json.get("discriminator")
    avatar = user_json.get("avatar")
    if avatar is None:
        avatar = "default"

    referral = request.cookies.get("referrer")
    if referral is not None:
        try:
            referral = int(referral)
        except ValueError:
            referral = None
        else:
            creation = (int(userid) >> 22) + 1420070400000
            if (time.time() * 1000) - creation < NEW_THRESHOLD:
                referral = None

    new_user = True
    if userid is not None:
        token = token_hex(64)
        user = User.query.get(userid)
        if user is not None:
            user.auth_token = token
            if user.discord_avatar != avatar:
                remove_old_avatar(user.discord_avatar)
                get_new_avatar(userid, avatar)

            new_user = False
        else:
            if User.query.filter_by(display_name=username).first() is not None:
                username = token_hex(8)

            get_new_avatar(userid, avatar)
            user = User(discord_user_id=userid, discord_discrim=discrim,
                        discord_avatar=avatar, display_name=username,
                        auth_token=token, isbanned=False,
                        isbeta=userid in beta_testers,
                        referrer=referral)
            db.session.add(user)
        db.session.commit()

        if new_user:
            response = make_response(redirect(url_for("ctf_page.onboarding")))
        else:
            response = make_response(redirect(url_for("ctf_page.home")))

        response.set_cookie("token", token, max_age=2_592_000)
        # Clear the referral
        response.set_cookie("referrer", "", expires=0)
        leaderboard.reload_all()
        return response
    else:
        return jsonify({"s": False, "m": "Invalid OAuth response"})


@api.route("/logout", methods=["GET"])
def logout():
    response = make_response(redirect("/"))
    response.set_cookie("token", "", max_age=0)
    return response


@api.route("/changeName", methods=["POST"])
@login_required
def changeName(user):
    name = request.form.get("name")
    oldname = user.display_name
    if not name.strip():
        return jsonify({"s": False, "m": "Name must not be whitespace"})
    if User.query.filter_by(display_name=name).first() is not None:
        return jsonify({"s": False, "m": "Name is already in use"})
    if len(name) > 26:
        return jsonify({"s": False, "m": "Name is longer than 40 characters"})
    if any(c not in printable for c in name):
        return jsonify(
            {"s": False, "m": "Name contains non-ASCII or non-printable characters"}
        )
    user.display_name = name
    db.session.commit()
    send_webhook(
        "Name changes",
        "Name changed",
        colour=0x00FF00,
        fields=[
            {'inline': True, 'name': 'Old', 'value': oldname},
            {'inline': True, 'name': 'New', 'value': name},
            {'inline': True, 'name': 'Discord ID', 'value': user.discord_user_id},
        ],
    )
    leaderboard.reload_all()
    return jsonify({"s": True, "m": "Name changed"})


@api.route("/attemptFlag", methods=["POST"])
@login_required
def attemptFlag(user):
    """Attempt a flag"""
    flag = request.form.get("flag")
    category = request.form.get("category")
    chal = request.form.get("challenge")
    if chal is None:
        return jsonify({"s": False, "m": "Invalid challenge"})
    chal = int(chal)
    flaghash = sha256_digest(flag).lower()
    challenge = Challenge.query.filter_by(flag=flaghash).first()
    if challenge is not None and not challenge.ishidden:
        categoryexpected = challenge.category
        chalexpected = challenge.challenge
        if category == categoryexpected and chal == chalexpected:
            if challenge in user.solved_challenges:
                return jsonify({"s": False, "m": "Flag already scored"})
            else:
                points = challenge.points
                if challenge in user.hint_used_challenges:
                    points /= 2
                username = user.display_name
                challenge_name = challenge.title
                first = len(challenge.users_solved) is 0
                scored_event = {
                    "event": "flag_score",
                    "username": username,
                    "challenge_name": challenge_name,
                    "isFirst": first,
                }

                stm = SolveTimeModel(user_id=user.discord_user_id,
                                     category=challenge.category,
                                     challenge=challenge.challenge,
                                     solve_time=datetime.utcnow())
                db.session.add(stm)
                user.solved_challenges.append(challenge)

                db.session.commit()
                print("Someone scored! " + str(scored_event))
                emit("flag_score", scored_event, namespace="/", broadcast=True)
                send_webhook(
                    "Flag Attempt",
                    "Successful attempt",
                    colour=0x00FF00,
                    fields=[
                        {'inline': True, 'name': 'Challenge', 'value': scored_event['challenge_name']},
                        {'inline': True, 'name': 'Username', 'value': scored_event['username']},
                        {'inline': True, 'name': 'Is First', 'value': str(scored_event.get('isFirst', None))},
                    ],
                )

                leaderboard.reload_all()
                update_leaderboard()
                usrpoints = leaderboard.points_for(user)
                solved = int(usrpoints / sum(i.points for i in Challenge.query.all()) * 100)
                if solved is 100:
                    complete_event = {
                    "event": "complete",
                    "username": username,
                    }
                    emit("complete", complete_event, namespace="/", broadcast=True)
                    send_webhook(
                        "Completion",
                        "User achieved 100%",
                        colour=0x0000FF,
                        fields=[
                            {'inline': True, 'name': 'Username', 'value': user.display_name},
                        ],
                    )
                return jsonify({"s": True, "m": f"Flag correct, +{int(points)} points", "f": first})
    else:
        challenge = Challenge.query.filter_by(category=category, challenge=chal).first()

    send_webhook(
        "Flag Attempt",
        "Unsuccessful attempt",
        colour=0xFF0000,
        fields=[
            {'inline': True, 'name': 'Challenge', 'value': challenge.title if challenge else '[Unknown]'},
            {'inline': True, 'name': 'Username', 'value': user.display_name},
            # Note: f-strings won't work here because of the backslash
            {'inline': True, 'name': 'Flag', 'value': "`" + flag.replace('`', '') + "`"},
        ],
    )
    return jsonify({"s": False, "m": "Incorrect Flag."})


@api.route("/attemptFlog", methods=["POST"])
@login_required
def attemptFlog(user):
    """Attempt a hidden flag"""
    flag = request.form.get("flag")
    flaghash = sha256_digest(flag).lower()
    challenge = Challenge.query.filter_by(flag=flaghash).first()

    if challenge is not None and challenge.ishidden:
        if challenge in user.solved_challenges:
            return jsonify({"s": False, "m": "Flag already scored"})
        else:
            points = challenge.points
            if challenge in user.hint_used_challenges:
                points /= 2
            username = user.display_name
            challenge_name = challenge.title
            first = len(challenge.users_solved) is 0
            scored_event = {
                "event": "flag_score",
                "username": username,
                "challenge_name": challenge_name,
                "isFirst": first,
            }
            stm = SolveTimeModel(user_id=user.discord_user_id,
                                     category=challenge.category,
                                     challenge=challenge.challenge,
                                     solve_time=datetime.utcnow())
            db.session.add(stm)
            user.solved_challenges.append(challenge)

            redirect = f"/category/{challenge.category}/{challenge.challenge}"

            db.session.commit()
            print("Someone scored! " + str(scored_event))
            emit("flag_score", scored_event, namespace="/", broadcast=True)
            send_webhook(
                "Hidden Flag Attempt",
                "Successful attempt",
                colour=0x00FF00,
                fields=[
                    {'inline': True, 'name': 'Challenge', 'value': scored_event['challenge_name']},
                    {'inline': True, 'name': 'Username', 'value': scored_event['username']},
                    {'inline': True, 'name': 'Is First', 'value': str(scored_event.get('isFirst', None))},
                ],
            )

            leaderboard.reload_all()
            update_leaderboard()
            return jsonify({"s": True, "m": f"Flag correct, +{int(points)} points", "f": first, "r": redirect})

    send_webhook(
        "Hidden Flag Attempt",
        "Unsuccessful attempt",
        colour=0xFF0000,
        fields=[
            # Note: f-strings won't work here because of the backslash
            {'inline': True, 'name': 'Flag', 'value': "`" + flag.replace('`', '') + "`"},
        ],
    )
    return jsonify({"s": False, "m": "Incorrect Flag."})


@api.route("/hint", methods=["POST"])
@login_required
def getHint(user):
    category = request.form.get("category")
    chal = request.form.get("challenge")
    chal = Challenge.query.filter_by(category=category, challenge=chal).first()
    if chal is None:
        return jsonify({"s": False, "m": "Invalid request"})
    if chal not in user.solved_challenges and chal not in user.hint_used_challenges:
        user.hint_used_challenges.append(chal)
        db.session.commit()
        send_webhook(
            "Hint Use",
            "User Used Hint",
            f"{user.display_name} used the hint for {chal.title}",
        )
    return jsonify({"s": True, "m": chal.hint.format(**formatting)})


@api.route("/admin/alerts/add", methods=["POST"])
@admin_only
@login_required
def add_alert(user):
    """Sends out an alert"""
    alert_body = request.form.get("body")
    alert_title = request.form.get("title")

    if not (alert_body and alert_title):
        return abort(400)

    alert_data = dict(title=alert_title, content=alert_body, uuid=uuid.uuid4().hex)
    alerts.append(alert_data)
    news.append(alert_data)
    emit("alert", alert_data, namespace="/", broadcast=True)
    send_webhook(
        "Alerts",
        "Alert added",
        colour=0x00FF00,
        fields=[
            {'inline': True, 'name': 'Title', 'value': alert_title},
            {'inline': True, 'name': 'Body', 'value': alert_body},
        ],
    )
    return jsonify(alerts)


@api.route("/admin/alerts/remove", methods=["POST"])
@admin_only
@login_required
def remove_alert_endpoint(user):
    """Removes an alert based on an alert title"""
    alert_title = request.form.get("title")
    if not alert_title:
        return abort(400)
    alerts = remove_alert(alert_title)
    send_webhook(
        "Alerts",
        "Alert removed",
        colour=0xFF0000,
        fields=[
            {'inline': True, 'name': 'Title', 'value': alert_title},
        ],
    )
    return jsonify(alerts)


def remove_alert(alert_title):
    for alert in alerts:
        if alert["title"] == alert_title:
            alerts.remove(alert)
    return alerts

@api.route("/admin/ban", methods=["POST"])
@admin_only
@login_required
def ban_user(user):
    user = request.form.get("uid")
    if not user:
        return abort(400)
    try:
        user = int(user)
    except ValueError:
        return abort(400)

    if user in admins:
        # Let's not go banning admins lol
        return abort(401)

    banuser = User.query.filter_by(discord_user_id=user).first()
    if banuser is None:
        return abort(400)

    banuser.isbanned = True
    db.session.commit()
    send_webhook(
        "Bans",
        "User banned",
        colour=0xFF0000,
        fields=[
            {'inline': True, 'name': 'Name', 'value': banuser.display_name},
            {'inline': True, 'name': 'Discord ID', 'value': banuser.discord_user_id},
        ],
    )
    return "200"

@api.route("/admin/unban", methods=["POST"])
@admin_only
@login_required
def unban_user(user):
    user = request.form.get("uid")
    if not user:
        return abort(400)
    try:
        user = int(user)
    except ValueError:
        return abort(400)

    banuser = User.query.filter_by(discord_user_id=user).first()
    if banuser is None:
        return abort(400)

    banuser.isbanned = False
    db.session.commit()
    send_webhook(
        "Bans",
        "User unbanned",
        colour=0x00FF00,
        fields=[
            {'inline': True, 'name': 'Name', 'value': banuser.display_name},
            {'inline': True, 'name': 'Discord ID', 'value': banuser.discord_user_id},
        ],
    )
    return "200"


@api.route("/admin/remc", methods=["POST"])
@admin_only
@login_required
def admin_remc(user):
    category = request.form.get("category")
    chal = request.form.get("challenge")
    user = request.form.get("uid")
    if chal is None:
        return jsonify({"s": False, "m": "Invalid challenge"})
    chal = int(chal)
    user = int(user)

    user = User.query.filter_by(discord_user_id=user).first()
    if user is None:
        return jsonify({"s": False, "m": "Invalid user"})

    challenge = Challenge.query.filter_by(category=category, challenge=chal).first()
    if challenge is None:
        return jsonify({"s": False, "m": "Invalid challenge"})

    if challenge in user.solved_challenges:
        user.solved_challenges.remove(challenge)
        SolveTimeModel.query.filter_by(user_id=user.discord_user_id,
                                       challenge=challenge.challenge,
                                       category=challenge.category).delete()

        db.session.commit()
    leaderboard.reload_all()

    send_webhook(
        "Admin",
        "Challenge descored",
        colour=0x0000FF,
        fields=[
            {'inline': True, 'name': 'Challenge', 'value': challenge.title},
            {'inline': True, 'name': 'Username', 'value': user.display_name},
        ],
    )


    return jsonify({"s": True, "m": ""})

