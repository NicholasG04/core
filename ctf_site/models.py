from flask_sqlalchemy import SQLAlchemy
from .config import admins

db = SQLAlchemy()

challenge_categories_rosetta = {
    "crypto": "Cryptography",
    "misc": "Miscellaneous",
    "reveng": "Reverse Engineering",
    "steg": "Steganography",
    "linux": "GNU+Linux",
    "web": "World Wide Web",
}

solved = db.Table(
    "solved",
    db.Column(
        "user_id", db.Integer, db.ForeignKey("user.discord_user_id"), primary_key=True
    ),
    db.Column("challenge_category", db.Text, primary_key=True),
    db.Column("challenge_number", db.Integer, primary_key=True),
    db.ForeignKeyConstraint(
        ["challenge_category", "challenge_number"],
        ["challenge.category", "challenge.challenge"],
    ),
)

hints = db.Table(
    "hints",
    db.Column(
        "user_id", db.Integer, db.ForeignKey("user.discord_user_id"), primary_key=True
    ),
    db.Column("challenge_category", db.Text, primary_key=True),
    db.Column("challenge_number", db.Integer, primary_key=True),
    db.ForeignKeyConstraint(
        ["challenge_category", "challenge_number"],
        ["challenge.category", "challenge.challenge"],
    ),
)


class TicketMessage(db.Model):
    ticket_id = db.Column(db.Integer, primary_key=True)
    message_num = db.Column(db.Integer, primary_key=True)
    author_id = db.Column(db.String)
    author_name = db.Column(db.String)
    message_content = db.Column(db.String)


class Ticket(db.Model):
    ticket_id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, nullable=False)
    ticket_status = db.Column(db.String, default="open")
    ticket_title = db.Column(db.String, nullable=False)


class Challenge(db.Model):
    category = db.Column(db.Text, primary_key=True)
    challenge = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text, nullable=False)
    description = db.Column(db.Text, nullable=False)
    flag = db.Column(db.Text, nullable=False)
    points = db.Column(db.Integer, nullable=False)
    hint = db.Column(db.Text, nullable=False)
    flag_format = db.Column(db.Text, nullable=False)
    ishidden = db.Column(db.Boolean, nullable=False)

    @property
    def first(self) -> str:
        from .leaderboard import leaderboard
        return leaderboard.get_first_solve(self)


class User(db.Model):
    discord_user_id = db.Column(db.Integer, primary_key=True)
    discord_discrim = db.Column(db.Integer, nullable=False)
    discord_avatar = db.Column(db.Text, nullable=False)
    display_name = db.Column(db.Text, nullable=False)
    auth_token = db.Column(db.Integer, nullable=False)
    solved_challenges = db.relationship(
        "Challenge",
        secondary=solved,
        lazy=True,
        backref=db.backref("users_solved", lazy=True),
    )
    hint_used_challenges = db.relationship(
        "Challenge",
        secondary=hints,
        lazy=True,
        backref=db.backref("users_hint_used", lazy=True),
    )
    isbeta = db.Column(db.Boolean, nullable=False)
    isbanned = db.Column(db.Boolean, nullable=False)
    isdonor = db.Column(db.Boolean, nullable=True)
    isadmin = discord_user_id in admins

    referrer = db.Column(db.Integer, nullable=True)

    def points(self):
        from .leaderboard import leaderboard
        return leaderboard.points_for(self)

    @property
    def first_bloods(self):
        from .leaderboard import leaderboard
        return leaderboard.get_first_bloods(self)

    @property
    def extreme_count(self):
        from .leaderboard import leaderboard
        return leaderboard.get_extreme_count(self)

    @property
    def easter_egg_count(self):
        from .leaderboard import leaderboard
        return leaderboard.get_easter_egg_count(self)

    def referrals(self):
        return User.query.filter_by(referrer=self.discord_user_id).count()


class SolveTimeModel(db.Model):
    user_id = db.Column(db.Integer, db.ForeignKey(User.discord_user_id), primary_key=True)
    category = db.Column(db.Text, db.ForeignKey(Challenge.category), primary_key=True)
    challenge = db.Column(db.Integer, db.ForeignKey(Challenge.challenge), primary_key=True)
    solve_time = db.Column(db.DateTime, nullable=False)
