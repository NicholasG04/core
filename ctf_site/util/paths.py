import os


def resolve_path(*path):
    return os.path.join(os.path.abspath(os.path.dirname(__file__)), "..", *path)
